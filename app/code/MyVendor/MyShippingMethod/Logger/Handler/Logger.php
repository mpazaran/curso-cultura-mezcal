<?php

namespace MyVendor\MyShippingMethod\Logger\Handler;

use Magento\Framework\Logger\Handler\Base;
use Monolog\Logger as MonoLogger;

class Logger extends Base
{

    /**
     * @var string
     */
    protected $fileName = '/var/log/my_vendor_my_payment_method.log';

    /**
     * @var
     */
    protected $loggerType = MonoLogger::DEBUG;

}
