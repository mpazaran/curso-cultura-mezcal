define(
    [
        'uiComponent',
        'Magento_Checkout/js/model/payment/renderer-list'
    ],
    function (Component,
              rendererList) {
        'use strict';
        rendererList.push(
            {
                type: 'my_broker',
                component: 'MyVendor_MyPaymentMethod/js/view/payment/method-renderer/my_broker'
            }
        );
        return Component.extend({});
    }
);

