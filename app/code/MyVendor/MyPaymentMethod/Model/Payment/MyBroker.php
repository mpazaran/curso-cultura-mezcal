<?php

/** @noinspection PhpDeprecationInspection */

namespace MyVendor\MyPaymentMethod\Model\Payment;

use Magento\Directory\Helper\Data as DirectoryHelper;
use Magento\Framework\App\Area;
use Magento\Framework\App\State;
use Magento\Payment\Model\Method\AbstractMethod;

class MyBroker extends AbstractMethod
{
    const CODE = 'my_broker';

    /**
     * Payment method code
     *
     * @var string
     */
    protected $_code = self::CODE;

    /**
     * @var string
     */
    protected $_formBlockType = \MyVendor\MyPaymentMethod\Block\Payment\MyBroker\Form::class;

    /**
     * @var string
     */
    protected $_infoBlockType = \MyVendor\MyPaymentMethod\Block\Payment\MyBroker\Info::class;

    /**
     * Availability option
     *
     * @var bool
     */
    protected $_isOffline = true;

    /**
     * @var State
     */
    protected $state;

    /**
     * @param \Magento\Framework\Model\Context                             $context
     * @param \Magento\Framework\Registry                                  $registry
     * @param \Magento\Framework\Api\ExtensionAttributesFactory            $extensionFactory
     * @param \Magento\Framework\Api\AttributeValueFactory                 $customAttributeFactory
     * @param \Magento\Payment\Helper\Data                                 $paymentData
     * @param \Magento\Framework\App\Config\ScopeConfigInterface           $scopeConfig
     * @param \Magento\Payment\Model\Method\Logger                         $logger
     * @param \Magento\Framework\Model\ResourceModel\AbstractResource|null $resource
     * @param \Magento\Framework\Data\Collection\AbstractDb|null           $resourceCollection
     * @param array                                                        $data
     * @param State                                                        $state
     * @param DirectoryHelper|null                                         $directory
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Api\ExtensionAttributesFactory $extensionFactory,
        \Magento\Framework\Api\AttributeValueFactory $customAttributeFactory,
        \Magento\Payment\Helper\Data $paymentData,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Payment\Model\Method\Logger $logger,
        State $state,
        \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null,
        \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
        array $data = [],
        DirectoryHelper $directory = null
    ) {
        parent::__construct(
            $context,
            $registry,
            $extensionFactory,
            $customAttributeFactory,
            $paymentData,
            $scopeConfig,
            $logger,
            $resource,
            $resourceCollection,
            $data,
            $directory
        );
        $this->state = $state;
    }

    public function isActive($storeId = null)
    {
        if ($this->state->getAreaCode() === Area::AREA_FRONTEND) {
            return (bool)$this->getConfigData('active', $storeId) && !((bool)$this->getConfigData('only_backend', $storeId));
        }

        return (bool)$this->getConfigData('active', $storeId);
    }


}
