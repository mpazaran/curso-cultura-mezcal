<?php

namespace MyVendor\MyPaymentMethod\Block\Payment\MyBroker;

class Info extends \Magento\Payment\Block\Info
{

    /**
     * @var string
     */
    protected $_template = 'MyVendor_MyPaymentMethod::payment/my_broker/info.phtml';

}


