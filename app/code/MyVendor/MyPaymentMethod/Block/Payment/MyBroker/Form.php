<?php

namespace MyVendor\MyPaymentMethod\Block\Payment\MyBroker;

class Form extends \Magento\Payment\Block\Form
{
    /**
     * Checkmo template
     *
     * @var string
     */
    protected $_template = 'MyVendor_MyPaymentMethod::payment/my_broker/form.phtml';
}


