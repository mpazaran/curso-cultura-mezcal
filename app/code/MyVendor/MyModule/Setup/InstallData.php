<?php

namespace MyVendor\MyModule\Setup;

use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;

class InstallData implements InstallDataInterface
{

    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        $table = $setup->getTable('curso_stores');

        $setup->getConnection()->insert(
            $table,
            [
                'id'      => null,
                'name'    => 'Roma Norte',
                'address' => 'Avenida Alvaro Obregon 64 Roma Norte',
                'phone'   => '55 7822 6622',
            ]
        );

        $setup->endSetup();

    }
}
