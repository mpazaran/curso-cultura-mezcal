<?php

namespace MyVendor\MyModule\Setup;

use Magento\Framework\DB\Adapter\AdapterInterface;
use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

class InstallSchema implements InstallSchemaInterface
{

    /**
     * @inheritdoc
     * @throws \Zend_Db_Exception
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        $connection = $setup->getConnection();

        $tableName = $setup->getTable('curso_stores');

        if ($connection->isTableExists($tableName)) {
            $connection->dropTable($tableName);
        }

        $table = $connection->newTable($tableName)
            ->addColumn(
                'id',
                Table::TYPE_INTEGER,
                null,
                [
                    Table::OPTION_NULLABLE  => false,
                    Table::OPTION_IDENTITY  => true,
                    Table::OPTION_UNSIGNED  => true,
                ],
                'ID'
            )
            ->addColumn(
                'name',
                Table::TYPE_TEXT,
                255,
                [
                    Table::OPTION_NULLABLE  => false,
                    Table::OPTION_IDENTITY  => false,
                    Table::OPTION_LENGTH    => 255,
                ],
                'Name'
            )
            ->addColumn(
                'address',
                Table::TYPE_TEXT,
                255,
                [
                    Table::OPTION_NULLABLE  => false,
                    Table::OPTION_LENGTH    => 255,
                ],
                'Address'
            )
            ->addColumn(
                'phone',
                Table::TYPE_TEXT,
                25,
                [
                    Table::OPTION_NULLABLE  => false,
                    Table::OPTION_LENGTH    => 25,
                ],
                'Phone'
            )->addIndex(
                'idx_primary',
                ['id'],
                ['type' => AdapterInterface::INDEX_TYPE_PRIMARY]
            );
        $connection->createTable($table);

        $setup->endSetup();
    }
}
