<?php

namespace MyVendor\MyModule\Model\ResourceModel;

/**
 * Relates the mysql table curso_stores with the model \MyVendor\MyModule\Model\CursoStores.
 *
 */
class CursoStores extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{

    /**
     * @inheritdoc
     */
    public function __construct(\Magento\Framework\Model\ResourceModel\Db\Context $context)
    {
        parent::__construct($context);
    }

    /**
     * @inheritdoc
     */
    protected function _construct()
    {
        $this->_init('curso_stores', 'id');
    }
}