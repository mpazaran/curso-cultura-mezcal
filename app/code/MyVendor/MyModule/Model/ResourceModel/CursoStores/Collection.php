<?php

namespace MyVendor\MyModule\Model\ResourceModel\CursoStores;

/**
 * Collection for \MyVendor\MyModule\Model\CursoStores.
 *
 */
class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    /**
     * Identifier field name for collection items
     *
     * Can be used by collections with items without defined
     *
     * @var string
     */
    protected $_idFieldName = 'id';

    /**
     * Name prefix of events that are dispatched by model
     *
     * @var string
     */
    protected $_eventPrefix = 'my_vendor_my_module_curso_stores_collection';

    /**
     * Name of event parameter
     *
     * @var string
     */
    protected $_eventObject = 'my_vendor_my_module_curso_stores_collection';

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(\MyVendor\MyModule\Model\CursoStores::class, \MyVendor\MyModule\Model\ResourceModel\CursoStores::class);
    }

}
