<?php

namespace MyVendor\MyModule\Model\ResourceModel\Shops;

use Magento\Framework\Data\Collection\Db\FetchStrategyInterface as FetchStrategy;
use Magento\Framework\Data\Collection\EntityFactoryInterface as EntityFactory;
use Magento\Framework\Event\ManagerInterface as EventManager;
use Magento\Framework\View\Element\UiComponent\DataProvider\SearchResult;
use MyVendor\MyModule\Model\ResourceModel\CursoStores as ResourceModel;
use Psr\Log\LoggerInterface as Logger;

class ListCollection extends SearchResult
{
    public function __construct(
        EntityFactory $entityFactory,
        Logger $logger,
        FetchStrategy $fetchStrategy,
        EventManager $eventManager,
        ResourceModel $resourceModel,
        $identifierName = null,
        $connectionName = null
    ) {
        parent::__construct($entityFactory,
            $logger,
            $fetchStrategy,
            $eventManager,
            $resourceModel->getMainTable(),
            ResourceModel::class,
            $identifierName, $connectionName);
    }

    /**
     * Initialization here
     *
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();

    }

    /**
     * Here you can write the joins
     * @return SearchResult|void
     */
    protected function _initSelect()
    {
        parent::_initSelect();

    }

}
