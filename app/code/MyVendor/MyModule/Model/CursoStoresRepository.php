<?php

namespace MyVendor\MyModule\Model;

use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Api\SortOrderBuilder;
use Magento\Framework\Exception\NoSuchEntityException;
use MyVendor\MyModule\Api\CursoStoresRepositoryInterface as ApiCursoStoresRepositoryInterface;
use MyVendor\MyModule\Api\CursoStoresSearchResultsInterfaceFactory as ApiCursoStoresSearchResultsInterface;
use MyVendor\MyModule\Model\ResourceModel\CursoStores as ResourceModelCursoStores;
use MyVendor\MyModule\Model\ResourceModel\CursoStoresFactory as ResourceModelCursoStoresFactory;


/**
 * This is the repository for CursoStores
 *
 * @package MyVendor\MyModule\Model\repository
 */
class CursoStoresRepository implements ApiCursoStoresRepositoryInterface
{

    /**
     * @var CursoStoresFactory
     */
    protected $modelFactory = null;

    /**
     * @var ResourceModelCursoStoresFactory
     */
    protected $resourceModelFactory = null;

    /**
     * @var ResourceModelCursoStores
     */
    protected $resourceModel = null;

    /**
     * @var \MyVendor\MyModule\Api\CursoStoresSearchResultsInterfaceFactory
     */
    protected $searchResultsFactory;

    /**
     * @var ResourceModelCursoStores\CollectionFactory
     */
    protected $collectionFactory = null;

    /**
     * @var CollectionProcessorInterface
     */
    private $collectionProcessor;

    /**
     * @var SearchCriteriaBuilder
     */
    private $searchCriteriaBuilder;

    /**
     * @var SortOrderBuilder
     */
    private $sortOrderBuilder;

    /**
     * @param CursoStoresFactory                         $modelFactory
     * @param ResourceModelCursoStoresFactory            $resourceModelFactory
     * @param ResourceModelCursoStores\CollectionFactory $collectionFactory
     * @param ApiCursoStoresSearchResultsInterface       $searchResultsFactory
     * @param CollectionProcessorInterface               $collectionProcessor
     * @param SearchCriteriaBuilder                      $searchCriteriaBuilder
     * @param SortOrderBuilder                           $sortOrderBuilder
     */
    public function __construct(
        CursoStoresFactory $modelFactory,
        ResourceModelCursoStoresFactory $resourceModelFactory,
        ResourceModelCursoStores\CollectionFactory $collectionFactory,
        ApiCursoStoresSearchResultsInterface $searchResultsFactory,
        CollectionProcessorInterface $collectionProcessor,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        SortOrderBuilder $sortOrderBuilder
    ) {
        $this->modelFactory          = $modelFactory;
        $this->resourceModelFactory  = $resourceModelFactory;
        $this->resourceModel         = $resourceModelFactory->create();
        $this->searchResultsFactory  = $searchResultsFactory;
        $this->collectionFactory     = $collectionFactory;
        $this->collectionProcessor   = $collectionProcessor;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->sortOrderBuilder      = $sortOrderBuilder;
    }

    /**
     * @return \MyVendor\MyModule\Api\Data\CursoStoresInterface
     */
    public function create()
    {
        return $this->modelFactory->create();
    }

    /**
     * @param SearchCriteriaInterface $searchCriteria
     *
     * @return \MyVendor\MyModule\Api\CursoStoresSearchResultsInterface
     */
    public function getList(SearchCriteriaInterface $searchCriteria = null)
    {
        $searchResults = $this->searchResultsFactory->create();
        $collection    = $this->getCollection();
        if (null !== $searchCriteria) {
            $searchResults->setSearchCriteria($searchCriteria);
            $this->collectionProcessor->process($searchCriteria, $collection);
        }
        $searchResults->setTotalCount($collection->getSize());

        return $searchResults->setItems($collection->getItems());
    }

    /**
     * @return \MyVendor\MyModule\Model\ResourceModel\CursoStores\Collection
     */
    public function getCollection()
    {
        return $this->collectionFactory->create();
    }

    /**
     * @param mixed  $value
     * @param string $field
     *
     * @return \MyVendor\MyModule\Api\Data\CursoStoresInterface
     * @throws NoSuchEntityException
     */
    public function load($value, $field = null)
    {
        return $this->loadModel($this->modelFactory->create(), $value, $field);
    }

    /**
     * @param \MyVendor\MyModule\Api\Data\CursoStoresInterface $model
     * @param mixed                                            $value
     * @param string                                           $field
     *
     * @return \MyVendor\MyModule\Api\Data\CursoStoresInterface
     * @throws NoSuchEntityException
     */
    public function loadModel(\MyVendor\MyModule\Api\Data\CursoStoresInterface $model, $value, $field = null)
    {
        $this->resourceModel->load($model, $value, $field);
        if (!$model->getId()) {
            throw new NoSuchEntityException(__('The register not longer exists.'));
        }

        return $model;
    }

    /**
     * @param \MyVendor\MyModule\Api\Data\CursoStoresInterface $model
     *
     * @return \MyVendor\MyModule\Api\Data\CursoStoresInterface
     * @throws \Exception
     */
    public function save(\MyVendor\MyModule\Api\Data\CursoStoresInterface $model)
    {
        $this->resourceModel->save($model);

        return $model;
    }

    /**
     * @param \MyVendor\MyModule\Api\Data\CursoStoresInterface $model
     *
     * @return \MyVendor\MyModule\Api\Data\CursoStoresInterface
     * @throws \Exception
     */
    public function delete(\MyVendor\MyModule\Api\Data\CursoStoresInterface $model)
    {
        $this->resourceModel->delete($model);

        return $model;
    }

    /**
     * @param int $id
     *
     * @return \MyVendor\MyModule\Api\Data\CursoStoresInterface
     * @throws NoSuchEntityException
     */
    public function deleteById($id)
    {
        $model = $this->load($id);
        $this->resourceModel->delete($model);

        return $model;
    }

    /**
     * @param SearchCriteriaInterface $searchCriteria
     *
     * @return array
     */
    public function toOptionArray(SearchCriteriaInterface $searchCriteria = null)
    {
        $options = [];
        foreach ($this->getList($searchCriteria)->getItems() as $model) {
            $options[] = [
                'value' => $model->getId(),
                'label' => $model->getName(),
            ];
        }

        return $options;
    }

}
