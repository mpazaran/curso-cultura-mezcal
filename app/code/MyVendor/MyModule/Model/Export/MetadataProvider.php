<?php

namespace MyVendor\MyModule\Model\Export;

use Magento\Framework\Api\Search\DocumentInterface;
use Magento\Framework\View\Element\UiComponentInterface;
use Magento\Ui\Model\Export\MetadataProvider as MagentoMetadataProvider;

class MetadataProvider extends MagentoMetadataProvider
{
    /**
     * @param \Magento\Framework\View\Element\UiComponentInterface $component
     *
     * @return array
     */
    public function getFields(\Magento\Framework\View\Element\UiComponentInterface $component)
    {
        $row = [];
        foreach ($this->getColumns($component) as $column) {
            if ($column->getData('export') === 'false') {
                continue;
            }
            if (null !== ($exportKey = $column->getData('exportIndex'))) {
                $row[] = $exportKey;
            } else {
                $row[] = $column->getName();
            }

        }

        return $row;
    }

    /**
     * Retrieve Headers row array for Export
     *
     * @param UiComponentInterface $component
     *
     * @return string[]
     */
    public function getHeaders(UiComponentInterface $component)
    {
        $row = [];
        foreach ($this->getColumns($component) as $column) {
            if ($column->getData('export') === 'false') {
                continue;
            }
            $row[] = $column->getData('config/label');
        }

        array_walk($row, function (&$header) {
            if (mb_strpos($header, 'ID') === 0) {
                $header = '"' . $header . '"';
            }
        });

        return $row;
    }

    /**
     * @param DocumentInterface $document
     * @param array             $fields
     * @param array             $options
     *
     * @return array
     */
    public function getRowData(DocumentInterface $document, $fields, $options)
    {
        $row = [];
        foreach ($fields as $column) {
            if (isset($options[$column])) {
                $key = $document->getCustomAttribute($column)->getValue();
                if (isset($options[$column][$key])) {
                    $row[] = $options[$column][$key];
                } else {
                    $row[] = '';
                }
            } else {
                $row[] = $document->getCustomAttribute($column)->getValue();
            }
        }

        return $row;
    }


}
