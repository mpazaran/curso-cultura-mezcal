<?php

namespace MyVendor\MyModule\Model;

use Magento\Framework\DataObject\IdentityInterface;
use Magento\Framework\Model\AbstractModel;
use MyVendor\MyModule\Api\Data\CursoStoresInterface;

/**
 * This is the table curso_stores representation in Magento.
 *
 * @package MyVendor\MyModule\Model
 */
class CursoStores extends AbstractModel implements IdentityInterface, CursoStoresInterface
{
    /**
     * Model cache tag for clear cache in after save and after delete
     */
    const CACHE_TAG = 'my_vendor_my_module_curso_stores';

    /**
     * Model cache tag for clear cache in after save and after delete
     *
     * When you use true - all cache will be clean
     *
     * @var string|array|bool
     */
    protected $_cacheTag = 'my_vendor_my_module_curso_stores';

    /**
     * Name of object id field
     *
     * @var string
     */
    protected $_eventPrefix = 'my_vendor_my_module_curso_stores';

    /**
     * @inheritdoc
     */
    protected function _construct()
    {
        $this->_init(ResourceModel\CursoStores::class);
    }

    /**
     * @inheritdoc
     */
    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    /**
     * @return array
     */
    public function getDefaultValues()
    {
        $values = [];
        return $values;
    }


    /**
     * @return int
     */
    public function getId()
    {
        return $this->getData('id');
    }

    /**
     * @param int $id
     *
     * @return CursoStoresInterface
     */
    public function setId($id)
    {
        return $this->setData('id', $id);
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->getData('name');
    }

    /**
     * @param string $name
     *
     * @return CursoStoresInterface
     */
    public function setName($name)
    {
        return $this->setData('name', $name);
    }

    /**
     * @return string
     */
    public function getAddress()
    {
        return $this->getData('address');
    }

    /**
     * @param string $address
     *
     * @return CursoStoresInterface
     */
    public function setAddress($address)
    {
        return $this->setData('address', $address);
    }

    /**
     * @return string
     */
    public function getPhone()
    {
        return $this->getData('phone');
    }

    /**
     * @param string $phone
     *
     * @return CursoStoresInterface
     */
    public function setPhone($phone)
    {
        return $this->setData('phone', $phone);
    }

}
