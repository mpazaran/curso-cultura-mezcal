<?php

namespace MyVendor\MyModule\Block\Adminhtml\Stores;

use Magento\Backend\Block\Template;
use MyVendor\MyModule\Logger\Logger;


class Index extends Template
{
    /**
     * @var \MyVendor\MyModule\Model\CursoStores
     */
    private $cursoStores;
    /**
     * @var Logger
     */
    private $logger;
    /**
     * @var \MyVendor\MyModule\Model\CursoStoresFactory
     */
    private $cursoStoresFactory;
    /**
     * @var \MyVendor\MyModule\Api\CursoStoresRepositoryInterface
     */
    public $cursoStoresRepository;
    /**
     * @var \Magento\Framework\App\ResourceConnection
     */
    private $resourceConnection;

    /**
     * Index constructor.
     *
     * @param Template\Context                                      $context
     * @param \MyVendor\MyModule\Model\CursoStores                  $cursoStores
     * @param \MyVendor\MyModule\Model\CursoStoresFactory           $cursoStoresFactory
     * @param \MyVendor\MyModule\Api\CursoStoresRepositoryInterface $cursoStoresRepository
     * @param \Magento\Framework\App\ResourceConnection             $resourceConnection
     * @param Logger                                                $logger
     * @param array                                                 $data
     */
    public function __construct(
        Template\Context $context,
        \MyVendor\MyModule\Model\CursoStores $cursoStores,
        \MyVendor\MyModule\Model\CursoStoresFactory $cursoStoresFactory,
        \MyVendor\MyModule\Api\CursoStoresRepositoryInterface $cursoStoresRepository,
        \Magento\Framework\App\ResourceConnection $resourceConnection,
        Logger $logger,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->cursoStores           = $cursoStores;
        $this->logger                = $logger;
        $this->cursoStoresFactory    = $cursoStoresFactory;
        $this->cursoStoresRepository = $cursoStoresRepository;
        $this->resourceConnection    = $resourceConnection;
    }

    /**
     * @return \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
     */
    public function getAllCursoStores()
    {
        return $this->cursoStores->getCollection()
            ->addFieldToFilter(
                [
                    'address',
                    'phone',
                ],
                [
                    ['like' => '%PLACE 571%'],
                    ['eq' => '5364056420']
                ]
            );
    }

    public function createCursoStore($data)
    {
        $curso = $this->cursoStoresFactory->create();

        $curso->setData($data);

        $curso->setName($data['name']);
        $curso->setAddress($data['address']);
        $curso->setPhone($data['phone']);

        $curso->save();
    }

    public function getList($searchCriteria)
    {
        return $this->cursoStoresRepository->getList($searchCriteria);
    }

    public function delete($model)
    {
        return $this->cursoStoresRepository->delete($model);
    }

    public function directQuery()
    {
        $connection = $this->resourceConnection->getConnection();
        //return $connection->query('SELECT `main_table`.* FROM `curso_stores` AS `main_table` where id = ?', [7])
        return $connection->query('SELECT `main_table`.* FROM `curso_stores` AS `main_table` where id = :id', ['id' => 7])
            ->fetchAll(\PDO::FETCH_ASSOC);
    }
}
