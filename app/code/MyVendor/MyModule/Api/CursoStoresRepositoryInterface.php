<?php

namespace MyVendor\MyModule\Api;

use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Data\OptionSourceInterface;
use Magento\Framework\Exception\NoSuchEntityException;

/**
 * @package MyVendor\MyModule\Api
 */
interface CursoStoresRepositoryInterface extends OptionSourceInterface
{

    /**
     * @return \MyVendor\MyModule\Api\Data\CursoStoresInterface
     */
    public function create();

    /**
     * @param SearchCriteriaInterface $searchCriteria
     *
     * @return \MyVendor\MyModule\Api\CursoStoresSearchResultsInterface
     */
    public function getList(SearchCriteriaInterface $searchCriteria = null);

    /**
     * @return \MyVendor\MyModule\Model\ResourceModel\CursoStores\Collection
     */
    public function getCollection();

    /**
     * @param mixed  $value
     * @param string $field
     *
     * @return \MyVendor\MyModule\Api\Data\CursoStoresInterface
     * @throws NoSuchEntityException
     */
    public function load($value, $field = null);

    /**
     * @param \MyVendor\MyModule\Api\Data\CursoStoresInterface $model
     * @param mixed  $value
     * @param string $field
     *
     * @return \MyVendor\MyModule\Api\Data\CursoStoresInterface
     * @throws NoSuchEntityException
     */
    public function loadModel(\MyVendor\MyModule\Api\Data\CursoStoresInterface $model, $value, $field = null);

    /**
     * @param \MyVendor\MyModule\Api\Data\CursoStoresInterface $model
     *
     * @return \MyVendor\MyModule\Api\Data\CursoStoresInterface
     */
    public function save(\MyVendor\MyModule\Api\Data\CursoStoresInterface $model);

    /**
     * @param \MyVendor\MyModule\Api\Data\CursoStoresInterface $model
     *
     * @return \MyVendor\MyModule\Api\Data\CursoStoresInterface
     */
    public function delete(\MyVendor\MyModule\Api\Data\CursoStoresInterface $model);

    /**
     * @param int $id
     *
     * @return \MyVendor\MyModule\Api\Data\CursoStoresInterface
     * @throws NoSuchEntityException
     */
    public function deleteById($id);

    /**
     * @param SearchCriteriaInterface $searchCriteria
     *
     * @return array
     */
    public function toOptionArray(SearchCriteriaInterface $searchCriteria = null);

}
