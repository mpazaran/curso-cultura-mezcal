<?php

namespace MyVendor\MyModule\Api;

use Magento\Framework\Api\SearchResultsInterface;

interface CursoStoresSearchResultsInterface extends SearchResultsInterface
{
    /**
     * Get customer groups list.
     *
     * @return \MyVendor\MyModule\Api\Data\CursoStoresInterface[]
     */
    public function getItems();

    /**
     * Set customer groups list.
     *
     * @api
     * @param \MyVendor\MyModule\Api\Data\CursoStoresInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}
