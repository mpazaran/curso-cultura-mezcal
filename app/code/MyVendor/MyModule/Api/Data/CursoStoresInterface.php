<?php

namespace MyVendor\MyModule\Api\Data;

/**
 * @package MyVendor\MyModule\Api
 */
interface CursoStoresInterface
{

    /**
     * @param array $fields []
     *
     * @return array
     */
    public function toArray(array $fields = []);

    /**
     * @return int
     */
    public function getId();

    /**
     * @param int $id
     *
     * @return CursoStoresInterface
     */
    public function setId($id);

    /**
     * @return string
     */
    public function getName();

    /**
     * @param string $name
     *
     * @return CursoStoresInterface
     */
    public function setName($name);

    /**
     * @return string
     */
    public function getAddress();

    /**
     * @param string $address
     *
     * @return CursoStoresInterface
     */
    public function setAddress($address);

    /**
     * @return string
     */
    public function getPhone();

    /**
     * @param string $phone
     *
     * @return CursoStoresInterface
     */
    public function setPhone($phone);
}
