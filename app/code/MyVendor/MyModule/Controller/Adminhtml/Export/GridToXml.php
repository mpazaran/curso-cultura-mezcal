<?php

namespace MyVendor\MyModule\Controller\Adminhtml\Export;

use Magento\Ui\Controller\Adminhtml\Export\GridToXml as MagentoGridToXml;

/**
 * Controller to perform export
 */
class GridToXml extends MagentoGridToXml
{

    /**
     * Authorization level of a basic admin session
     *
     * @see _isAllowed()
     */
    const ADMIN_RESOURCE = 'MyVendor_MyModule::curso_stores_read';

}
