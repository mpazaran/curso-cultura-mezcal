<?php

namespace MyVendor\MyModule\Controller\Adminhtml\Export;

use Magento\Ui\Controller\Adminhtml\Export\GridToCsv as MagentoGridToCsv;

/**
 * Controller to perform export
 */
class GridToCsv extends MagentoGridToCsv
{

    /**
     * Authorization level of a basic admin session
     *
     * @see _isAllowed()
     */
    const ADMIN_RESOURCE = 'MyVendor_MyModule::curso_stores_read';

}
