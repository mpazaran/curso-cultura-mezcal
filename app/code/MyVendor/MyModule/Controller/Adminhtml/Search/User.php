<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

declare(strict_types=1);

namespace MyVendor\MyModule\Controller\Adminhtml\Search;

use Magento\Backend\App\Action\Context;
use Magento\Framework\Api\FilterBuilder;
use Magento\Framework\Api\Search\FilterGroupBuilder;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;use Magento\User\Model\ResourceModel\User\CollectionFactory as CollectionFactory;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\Api\SearchResults;
use Magento\Framework\Api\SortOrderBuilder;
use Magento\Framework\App\Action\HttpGetActionInterface;
use Magento\Framework\Controller\Result\Json;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\Controller\ResultInterface;
use Magento\User\Model\ResourceModel\User\Collection;
use Magento\User\Model\User as DataModel;

/**
 * Controller to search product for ui-select component
 */
class User extends \Magento\Backend\App\Action implements HttpGetActionInterface
{
    /**
     * Authorization level of a basic admin session
     *
     * @see _isAllowed()
     */
    const ADMIN_RESOURCE = 'MyVendor_MyModule::curso_stores_write';

    /**
     * @var JsonFactory
     */
    private $resultJsonFactory;

    /**
     * @var Collection
     */
    private $collection;

    /**
     * @var SearchCriteriaBuilder
     */
    private $searchCriteriaBuilder;

    /**
     * @var FilterBuilder
     */
    private $filterBuilder;

    /**
     * @var FilterGroupBuilder
     */
    private $filterGroupBuilder;

    /**
     * @var SortOrderBuilder
     */
    private $sortOrderBuilder;
    /**
     * @var CollectionProcessorInterface
     */
    private $collectionProcessor;

    /**
     * @param JsonFactory                  $resultFactory
     * @param CollectionFactory            $collectionFactory
     * @param SearchCriteriaBuilder        $searchCriteriaBuilder
     * @param FilterBuilder                $filterBuilder
     * @param FilterGroupBuilder           $filterGroupBuilder
     * @param SortOrderBuilder             $sortOrderBuilder
     * @param CollectionProcessorInterface $collectionProcessor
     * @param Context                      $context
     */
    public function __construct(
        JsonFactory $resultFactory,
        CollectionFactory $collectionFactory,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        FilterBuilder $filterBuilder,
        FilterGroupBuilder $filterGroupBuilder,
        SortOrderBuilder $sortOrderBuilder,
        CollectionProcessorInterface $collectionProcessor,
        Context $context
    ) {
        $this->resultJsonFactory = $resultFactory;

        parent::__construct($context);
        $this->collection            = $collectionFactory->create();
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->filterBuilder         = $filterBuilder;
        $this->filterGroupBuilder    = $filterGroupBuilder;
        $this->sortOrderBuilder      = $sortOrderBuilder;
        $this->collectionProcessor   = $collectionProcessor;
    }

    /**
     * Execute customer search.
     *
     * @return ResultInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function execute(): ResultInterface
    {

        $id        = $this->getRequest()->getParam('id');
        $searchKey = $this->getRequest()->getParam('searchKey');
        $pageNum   = (int)$this->getRequest()->getParam('page') ?? 1;
        $limit     = (int)$this->getRequest()->getParam('limit') ?? 50;

        $this->searchCriteriaBuilder
            ->setCurrentPage($pageNum)
            ->setPageSize($limit);

        if(!empty($id)){
            $this->searchCriteriaBuilder->addFilter('main_table.user_id', $id);
        } else {
            if (!empty($searchKey)) {
                $this->filterGroupBuilder->addFilter(
                    $this->filterBuilder->setField('firstname')
                        ->setValue('%' . $searchKey . '%')
                        ->setConditionType('like')
                        ->create()
                )->addFilter(
                    $this->filterBuilder->setField('lastname')
                        ->setValue('%' . $searchKey . '%')
                        ->setConditionType('like')
                        ->create()
                )->addFilter(
                    $this->filterBuilder->setField('email')
                        ->setValue('%' . $searchKey . '%')
                        ->setConditionType('like')
                        ->create()
                );
                $this->searchCriteriaBuilder->setFilterGroups([$this->filterGroupBuilder->create()]);
            }
        }

        $this->searchCriteriaBuilder->setSortOrders([
            $this->sortOrderBuilder->setField('firstname')->create(),
            $this->sortOrderBuilder->setField('lastname')->create(),
            $this->sortOrderBuilder->setField('email')->create()
        ]);

        $this->collectionProcessor->process(
            $this->searchCriteriaBuilder->create(),
            $this->collection
        );

        /** @var SearchResults $itemList */
        $totalValues = $this->collection->getSize();
        $items       = [];
        /** @var DataModel $item */
        foreach ($this->collection as $item) {
            $itemId         = $item->getId();
            $items[$itemId] = [
                'value'     => $itemId,
                'label'     => $item->getName() . ' <' . $item->getEmail() . '>',
                'is_active' => $item->getIsActive(),
                'path'      => $item->getEmail(),
                'optgroup'  => false
            ];
        }
        /** @var Json $resultJson */
        $resultJson = $this->resultJsonFactory->create();

        return $resultJson->setData([
            'options' => $items,
            'total'   => empty($items) ? 0 : $totalValues
        ]);
    }
}
