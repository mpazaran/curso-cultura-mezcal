<?php

namespace MyVendor\MyModule\Controller\Adminhtml\Shops;

use Magento\Backend\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;

class Index extends \Magento\Backend\App\Action
{

    /**
     * @var PageFactory
     */
    protected $resultPageFactory;

    /**
     * @param Context $context
     * @param PageFactory $resultPageFactory
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory
    ) {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
    }

    /**
     * @inheritdoc
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('MyVendor_MyModule::mymodule');
        $resultPage->addBreadcrumb(__('Tiendas'), __('Tiendas'));
        $resultPage->addBreadcrumb(__('Manage item'), __('Tiendas'));
        $resultPage->getConfig()->getTitle()->prepend(__('Tiendas'));

        return $resultPage;
    }
}
