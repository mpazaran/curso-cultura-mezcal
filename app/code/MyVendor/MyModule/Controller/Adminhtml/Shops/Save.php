<?php

namespace MyVendor\MyModule\Controller\Adminhtml\Shops;

use Magento\Backend\App\Action;
use MyVendor\MyModule\Api\CursoStoresRepositoryInterface as RepositoryInterface;
use MyVendor\MyModule\Logger\Logger;
USE Magento\Catalog\Model\ImageUploader;

class Save extends Action
{
   /**
    * @var MyVendor\MyModule\Api\CursoStoresRepositoryInterface
    */
   protected $repository;
    /**
     * @var Logger $logger
     */
    protected $logger;

    /**
     * @param Action\Context      $context
     * @param RepositoryInterface $repository
     * @param Logger              $logger
     */
    public function __construct(
        Action\Context $context,
        RepositoryInterface $repository,
        Logger $logger
    ) {
        parent::__construct($context);
        $this->repository    = $repository;
        $this->logger        = $logger;
    }

    /**
     * Save action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function execute()
    {
        $data = $this->getRequest()->getPostValue();

        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        if ($data) {

            $model = $this->repository->create('id');

            $id = $this->getRequest()->getParam('id');

            if ($id) {
                $this->repository->loadModel($model, $id);
            }

            try {

                $model
                                ->setName($data['name'] ?? null)
                ->setAddress($data['address'] ?? null)
                ->setPhone($data['phone'] ?? null);
                $this->repository->save($model);

                if($model->getId()){
                    $this->messageManager->addSuccessMessage(__('Tienda has been saved.'));
                    $this->_session->setData('myvendor_mymodule_shops_form_data', false);

                    if ($this->getRequest()->getParam('back')) {
                        return $resultRedirect->setPath('*/*/edit', ['id' => $model->getId(), '_current' => true]);
                    }

                    return $resultRedirect->setPath('*/*/');

                }

                $this->messageManager->addExceptionMessage($e, __('Something went wrong while saving %1.', 'Tienda'));

                $this->_session->setData('myvendor_mymodule_shops_form_data', $data);

                return $resultRedirect->setPath('*/*/new');

            } catch (\Magento\Framework\Exception\LocalizedException $e) {
                $this->logger->error($e->getMessage());
                $this->logger->error($e->getTraceAsString());
                $this->messageManager->addExceptionMessage($e, __('Something went wrong while delete %1.', 'Tienda'));
            } catch (\RuntimeException $e) {
                $this->logger->error($e->getMessage());
                $this->logger->error($e->getTraceAsString());
                $this->messageManager->addExceptionMessage($e, __('Something went wrong while delete %1.', 'Tienda'));
            } catch (\Exception $e) {
                $this->logger->error($e->getMessage());
                $this->logger->error($e->getTraceAsString());
                $this->messageManager->addExceptionMessage($e, __('Something went wrong while delete %1.', 'Tienda'));
            }

            $this->_session->setData('myvendor_mymodule_shops_form_data', $data);

            return $resultRedirect->setPath('*/*/edit', ['id' => $this->getRequest()->getParam('id')]);
        }

        return $resultRedirect->setPath('*/*/');
    }
}
