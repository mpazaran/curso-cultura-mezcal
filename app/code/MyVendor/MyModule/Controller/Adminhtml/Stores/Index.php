<?php

namespace MyVendor\MyModule\Controller\Adminhtml\Stores;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\View\Result\Page;
use Magento\Framework\View\Result\PageFactory;
use MyVendor\MyModule\Logger\Logger;

class Index extends Action
{

    /**
     * @var PageFactory
     */
    protected $resultPageFactory;
    /**
     * @var Logger
     */
    private $logger;

    /**
     * @param Context                           $context
     * @param PageFactory                       $resultPageFactory
     * @param Logger $logger
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
        Logger $logger
    ) {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
        $this->logger = $logger;
    }

    /**
     * @inheritdoc
     */
    public function execute()
    {
        /** @var Page $resultPage */
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('MyVendor_MyModule::mymodule');
        $resultPage->addBreadcrumb(__('Stores'), __('Stores'));
        $resultPage->addBreadcrumb(__('Manage item'), __('Stores'));
        $resultPage->getConfig()->getTitle()->prepend(__('Stores'));

        $this->logger->info('Se debe ocupar para denotar que sucedio una operación de forma correcta', [123,123,213]);
        $this->logger->warn('Es para posibles errores, pero que permiten que el proceso continue');
        $this->logger->err('Es para errores que no permiten que el proceso continue');
        $this->logger->debug('Es infomación de seguimiento');


        return $resultPage;
    }
}
