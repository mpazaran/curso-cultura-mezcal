<?php

namespace MyVendor\MyModule\Logger\Handler;

use Magento\Framework\Logger\Handler\Base;
use Monolog\Logger as MonoLogger;

class Logger extends Base
{

    /**
     * @var string
     */
    protected $fileName = '/var/log/my_vendor_my_module.log';

    /**
     * @var
     */
    protected $loggerType = MonoLogger::DEBUG;

}
